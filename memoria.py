#!/bin/env python

def int_to_big_endian(number):
    hex_number = hex(number)[2:]
    if len(hex_number) <= 8:
        res = ['0'*(8-len(hex_number)) + hex_number]
    else:
        if len(hex_number) % 8 != 0:
            hex_number = "0"*(8-(len(hex_number) % 8)) + hex_number
    n_palabras = len(hex_number) / 8
    res = ['' for i in range(n_palabras)]
    for i in range(n_palabras):
        res[i] = hex_number[8*i:(8*i)+8]
    return res

def int_to_little_endian(number):
    hex_number = hex(number)[2:]
    if len(hex_number) <= 8:
        res = [hex_number + '0'*(8-len(hex_number))]
    else:
        if len(hex_number) % 8 != 0:
            hex_number = hex_number + "0"*(8-(len(hex_number) % 8))
    n_palabras = len(hex_number) / 8
    res = [[] for i in range(n_palabras)]
    for i in range(n_palabras):
        res[n_palabras -1 - i] = hex_number[8*i:(8*i)+8]
    return res

def seq_to_big_endian(bytes_per_element, seq):
    digits = 2*bytes_per_element
    raw_seq = ''
    for i in seq:
        elem =hex(i)[2:]
        elem = elem +'0'*(digits - (len(elem))) # ajustar al tamaño de cada elemento
        raw_seq = raw_seq + elem
    n_palabras = len(raw_seq) / 8
    res = ['' for i in range(n_palabras)]
    for i in range(n_palabras):
        res[i] = hex_number[8*i:(8*i)+8]
    return res

def seq_to_little_endian(bytes_per_element, seq):
    digits = 2*bytes_per_element
    raw_seq = ''
    for i in seq:
        elem =hex(i)[2:]
        elem = '0'*(digits - (len(elem))) + elem # ajustar al tamaño de cada elemento
        raw_seq = raw_seq + elem
    n_palabras = len(raw_seq) / 8
    res = ['' for i in range(n_palabras)]
    for i in range(n_palabras):
        res[i] = hex_number[8*i:(8*i)+8]
    return res
